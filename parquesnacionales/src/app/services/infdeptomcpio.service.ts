import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InfdeptomcpioService {

  constructor(private http:HttpClient) { }

  getDeptos(){
    return this.http.get('http://127.0.0.1:8000/api/departamentos')
              .pipe(
                map((resp:any[]) =>
                   resp.map( depto =>({ departamento: depto.nombre,id: depto.id_departamento})
                  )
                )
              );
  }
  getMcpios(id:number){
    const url = `http://127.0.0.1:8000/api/municipios/${id}`;
    console.log(url);
    return this.http.get(url)
              .pipe(
                map((resp:any[]) =>
                   resp.map( mcpios =>({ municipio: mcpios.nombre,id: mcpios.id_municipio})
                  )
                )
              );

  }
  addItem(item:any){
    console.log(item);
    
    return this.http.post('http://127.0.0.1:8000/api/registros', {
        id_departamento: item.iddepto,
        id_municipio: item.idmcpio,
        nombre: item.nombre,
        fecha: item.fecha,
        correo: item.email
      }).pipe(
          map(resp =>{ return resp}
        )
      )

  }
  getUsuarios(){
    return this.http.get('http://127.0.0.1:8000/api/registros')
              .pipe(
                map((resp:any) => resp)
              );
  }
  borrarItem( id: number ) {
    const url = `http://127.0.0.1:8000/api/registros/${id}`;
    return this.http.delete(url);

  }


}
