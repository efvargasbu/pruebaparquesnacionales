import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup,Validators} from '@angular/forms'
import { InfdeptomcpioService } from './services/infdeptomcpio.service';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  pipe = new DatePipe('en-US');
  title = 'parquesnacionales';
  userForm:FormGroup;
  startDate = new Date(2010, 1, 1);
  minDate = new Date(2010, 1, 1); 
  maxDate = new Date(2019, 12, 31);
  deptos: any[] = [];
  mcpios: any[] = [];
  usuarios: any[] = [];
  itemdel = [];
  mensaje: string;
  error: string;
  iddel: any;

  constructor(private formBuilder: FormBuilder, private deptoService: InfdeptomcpioService){
    this.userForm = this.formBuilder.group({
      nombre: ['',Validators.required],
      iddepto: ['',Validators.required],
      idmcpio: ['',Validators.required],
      email:['',[Validators.required,
                 Validators.email,
                 Validators.pattern("[a-zA-Z0-9_.+-,;]+@(gmail\.com|parquesnacionales\.gov\.co)$")
                ]
            ],
      fecha:['',Validators.required]
 
    });
    this.listarUsuarios();
    
  }
  ngOnInit(){
    this.deptoService.getDeptos()
        .subscribe(deptos=>{
          this.deptos = deptos
          //console.log(this.deptos)
        });
  }

  submit(){
    
    const nfecha = this.pipe.transform(this.userForm.value.fecha,'yyyy-MM-dd');
    
    this.userForm.value.fecha= nfecha;
    
    this.deptoService.addItem(this.userForm.value)
        .subscribe((resp:any) => {
           console.log(resp)

          if(resp.errorCode==1){
            this.error=resp.message;
            this.mensaje='';
            console.error(this.error);
          }else{
            this.error='';
            this.mensaje=resp.message
            this.listarUsuarios();
          }
        });  

  }
  municipios(id:number){
    this.deptoService.getMcpios(id)
        .subscribe(mcpios=>{
          this.mcpios = mcpios;
          //console.log(this.mcpios)
        });
    
  }
  listarUsuarios(){
    this.deptoService.getUsuarios()
        .subscribe(users=>{

          this.usuarios = users;
          console.log(this.usuarios);  
        });
  }
  borrarItem( item: any, i: number ) {

    Swal.fire({
      title: '¿Está seguro?',
      text: 'Está seguro que desea borrar este item',
      showConfirmButton: true,
      showCancelButton: true
    }).then( resp => {

      if ( resp.value ) {
        this.usuarios.splice(i, 1);
        this.deptoService.borrarItem(item.id_registro).subscribe();
        //this.heroesService.borrarHeroe( heroe.id ).subscribe();
      }

    });

  }
  borrarItems(){

    //console.log(this.usuarios);  
    if(this.itemdel.length>0){

      this.error='';
      Swal.fire({
        title: '¿Está seguro?',
        text: 'Está seguro que desea borrar estos item',
        showConfirmButton: true,
        showCancelButton: true
      }).then( resp => {
        if ( resp.value ) {
          this.itemdel.forEach((item,index) =>{
            this.usuarios = this.usuarios.filter(usuario => usuario.id_registro != item);
            this.deptoService.borrarItem(item).subscribe();
          });
        }
      });
      
    }else{
      this.error="No hay items a eliminar";
    }
    //let element = <HTMLInputElement> document.getElementById("itemdel0");  
    //console.log(element.value);
  }
  checkChange(i,id){

    if (this.itemdel[i]){  
      this.itemdel.splice(i, 1);
    }
    else{
      this.itemdel[i] = id;
    }

  }
}
