<?php

namespace App;

use DB;

use Illuminate\Database\Eloquent\Model;

class Registro extends Model
{
    protected $table = 'registros';

    protected $fillable = ['id_departamento', 'id_municipio','nombre','fecha','correo','estado'];

    public function allJoin(){
    	$item_details = DB::table('registros')
		    ->join('departamentos', 'departamentos.id_departamento', '=', 'registros.id_departamento')
		    ->join('municipios', 'municipios.id_municipio', '=', 'registros.id_municipio')
		    ->select('registros.*', 'departamentos.nombre as departamento', 'municipios.nombre as municipio')
		    ->where('registros.estado','1')
		    ->get();

		return $item_details;    
    }
        
}
