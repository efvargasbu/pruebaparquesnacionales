<?php

namespace App\Http\Controllers;
use App\Registro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegistrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $registros = Registro::first()->allJoin();
          return response()->json($registros);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $data['message'] = '';
         $validator = Validator::make($request->all(), [
            'id_departamento' => 'required',
            'id_municipio' => 'required',
            'nombre' => 'required|min:3',
            'fecha' => 'required|date_format:Y-m-d|after:2010-01-01|before:2019-01-01',
            'correo' => 'required|email|unique:registros'
        ], [
            'id_departamento.required' => 'Error: debe seleccionar un Departamento.',
            'id_municipio.required'  => 'Error: debe seleccionar un Municipio.',
            'nombre.required'  => 'Error: debe digitar un nombre.',            
            'nombre.min'  => 'Error: la cantidad de caracteres del campo nombre debe ser mayor a 3.',            
            'fecha.required'  => 'Error: debe digitar una fecha.',
            'fecha.date_format'  => 'Error: el formato de la fecha no es el corrcecto.',
            'fecha.after'  => 'Error: el año debe ser mayor a 2010.',
            'fecha.before'  => 'Error: el año debe ser menor a 2019.',            
            'correo.required'  => 'Error: debe digitar un correo.',
            'correo.email'  => 'Error: debe digitar un correo valido.',
            'correo.unique'  => 'Error: el correo que intenta ingresar ya existe.'
        ]);
        if ($validator->fails()) {
            $data['errorCode'] = 1;
            foreach($validator->errors()->all() AS $id => $error) {
                $data['message'] .= $error . '<br>';
            }
            return response()->json($data);
        }

        $data['message'] = 'Se creó la actividad propuesta.';
        
        
        //Instanciamos la clase registros
        $registro = new Registro();
        //Declaramos el nombre con el nombre enviado en el request
        $registro->id_departamento = $request->id_departamento;
        $registro->id_municipio = $request->id_municipio;
        $registro->nombre = $request->nombre;
        $registro->fecha = $request->fecha;
        $registro->correo = $request->correo;
        //Guardamos el cambio en nuestro modelo
        $registro->save();
        
        $data['message'] = 'Se creó la actividad propuesta.';
        
        if($registro == false) {
            $data['message'] = 'Error al crear el registro.';
            $data['errorCode'] = 1;
        }
        return response()->json($data);       
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Solicitamos al modelo el registro con el id solicitado por GET.
        return Registro::where('id_registro', $id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {        
        return Registro::where('id_registro', $id)->update(['estado' => '0']);
        
        /*
        $registro = Registro::findOrFail('id_registro', $id);
        $registro->delete();        
        return view('dashboard')->with([
          'flash_message' => 'Deleted',
          'flash_message_important' => false
        ]);
         * 
         */
    }
}
