<?php

use Illuminate\Database\Seeder;

class DepartamentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departamentos')->delete();        
        $departamentos = array(
                    array('id_departamento' => '5','nombre' => 'ANTIOQUIA'),
                    array('id_departamento' => '8','nombre' => 'ATLÁNTICO'),
                    array('id_departamento' => '11','nombre' => 'BOGOTÁ, D.C.'),
                    array('id_departamento' => '13','nombre' => 'BOLÍVAR'),
                    array('id_departamento' => '15','nombre' => 'BOYACÁ'),
                    array('id_departamento' => '17','nombre' => 'CALDAS'),
                    array('id_departamento' => '18','nombre' => 'CAQUETÁ'),
                    array('id_departamento' => '19','nombre' => 'CAUCA'),
                    array('id_departamento' => '20','nombre' => 'CESAR'),
                    array('id_departamento' => '23','nombre' => 'CÓRDOBA'),
                    array('id_departamento' => '25','nombre' => 'CUNDINAMARCA'),
                    array('id_departamento' => '27','nombre' => 'CHOCÓ'),
                    array('id_departamento' => '41','nombre' => 'HUILA'),
                    array('id_departamento' => '44','nombre' => 'LA GUAJIRA'),
                    array('id_departamento' => '47','nombre' => 'MAGDALENA'),
                    array('id_departamento' => '50','nombre' => 'META'),
                    array('id_departamento' => '52','nombre' => 'NARIÑO'),
                    array('id_departamento' => '54','nombre' => 'NORTE DE SANTANDER'),
                    array('id_departamento' => '63','nombre' => 'QUINDIO'),
                    array('id_departamento' => '66','nombre' => 'RISARALDA'),
                    array('id_departamento' => '68','nombre' => 'SANTANDER'),
                    array('id_departamento' => '70','nombre' => 'SUCRE'),
                    array('id_departamento' => '73','nombre' => 'TOLIMA'),
                    array('id_departamento' => '76','nombre' => 'VALLE DEL CAUCA'),
                    array('id_departamento' => '81','nombre' => 'ARAUCA'),
                    array('id_departamento' => '85','nombre' => 'CASANARE'),
                    array('id_departamento' => '86','nombre' => 'PUTUMAYO'),
                    array('id_departamento' => '88','nombre' => 'ARCHIPIÉLAGO DE SAN ANDRÉS, PROVIDENCIA Y SANTA CATALINA'),
                    array('id_departamento' => '91','nombre' => 'AMAZONAS'),
                    array('id_departamento' => '94','nombre' => 'GUAINÍA'),
                    array('id_departamento' => '95','nombre' => 'GUAVIARE'),
                    array('id_departamento' => '97','nombre' => 'VAUPÉS'),
                    array('id_departamento' => '99','nombre' => 'VICHADA')
                  );
        DB::table('departamentos')->insert($departamentos);        
        
    }
}
