<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaRegistros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registros', function (Blueprint $table) {
            $table->bigIncrements('id_registro');
            $table->unsignedBigInteger('id_departamento');
            $table->unsignedBigInteger('id_municipio');            
            $table->string('nombre');        
            $table->date('fecha');
            $table->string('correo');                    
            $table->enum('estado', array('0','1'))->default('1');        
            $table->timestamps();
            $table->foreign('id_departamento')->references('id_departamento')->on('departamentos');
            $table->foreign('id_municipio')->references('id_municipio')->on('municipios');
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registros');
    }
}
